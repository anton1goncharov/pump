/***************************************************************************************************************
************************************************ CHARCURVES ****************************************************
****************************************************************************************************************/
CHARCURVES = {
    hasResizeableChart : true,
    dotRadius : 2.5,
    pointsOnLine : 2,
    zoomEpsilon  : 0.05,
    margin : {top: 18, right: 14, bottom: 52, left: 62},
    height0 : 600,
    aspect : 1.7,
    xMouseAxisRectHeight : 18,
    legendWidth : 150,
    epsilonForExtendingTheLineAlittleBitLeftAndRight : 0.03,
    xAxisLabelVerticalPositionAdjust : 12,
    yAxisLabelHorizontalAdjust : 20,
    xMouseAxisTextVerticalAdjust : 4,
    legendLine_1_fromTop : 20,
    legendLine_2_fromTop : 42,
    info: null,
    chosenBtnId : "",
    make: function(){
        var pumpsWithValidData = API.pumpsWithValidData;
        this.info = $("#charCurveContentInfo");
        this.zoomInButton = jQuery("#charCurveZoomIn");
        this.zoomOutButton = jQuery("#charCurveZoomOut");
        var buttons = "pump&nbsp; ";
        for (var i=0; i<pumpsWithValidData.length; i++){
            var pump = pumpsWithValidData[i];
            buttons += HtmlGen.eisBtn(pump, {id: "charCurveChoosePumpBtn" + pump, "class": "big"});
        }
        $("#charCurvePumpButtons").html(buttons);
        stretchButtonsAux("charCurveStretchButtonsParentTd", this);
        for (var i=0; i<pumpsWithValidData.length; i++){
            var pump = pumpsWithValidData[i];
            var id = "#charCurveChoosePumpBtn" + pump;
            var btnElt = $(id);
            btnElt.click(CHARCURVES.makePumpChangeHandler(id, pump));
        }
        if ( pumpsWithValidData.length ){
            $("#charCurveChoosePumpBtn" + pumpsWithValidData[0]).trigger("click");
        }
        zoomInit.call(this);
        var charCurveYRangeAutoStuffParentTd = jQuery("#charCurveYRangeAutoStuffParentTd")
        makeYAxisRangeStuff(charCurveYRangeAutoStuffParentTd, this.myName, this, {}, false, "yAxisManual");
    },
    setYaxisAuto: function(args){
        this.yAxisManual = false;
        console.log(this.myName + " set y axis auto");
    },
    setYaxisManual: function(args){
        this.yAxisManual = true;
        console.log(this.myName + " set y axis manual");
    },
    setYaxisAction: function(args){
        console.log(this.myName + " set y axis action");
    },
    makePumpChangeHandler: function(btnId, pump){
        return function(){
            if (CHARCURVES.chosenBtnId){
                $(CHARCURVES.chosenBtnId).removeClass("chosen");
            }
            CHARCURVES.pump = pump;
            CHARCURVES.chosenBtnId = btnId;
            $(btnId).addClass("chosen");
            // find out "when" (i.e., the indices into finalTable) the pump in question was running (solo or in combination)
            var codeForPump = Math.round(Math.pow(2, pump - 1));
            var soloRows = API.formulas.rowsWherePumpsRunOnly[codeForPump];
            var comboRows = API.formulas.rowsWherePumpRuns[pump].select(function(item){ return API.finalTable[item].pumpsList.length > 1; });
            // rows = API.formulas.rowsWherePumpsRun[codeForPump]; <-- that should be all the rows. Lets do a little test towards that end
            var shouldBeZero = comboRows.length + soloRows.length - API.formulas.rowsWherePumpRuns[pump].length;
            if (shouldBeZero) { console.log("data error"); debugger; }
            //CHARCURVES.info.html(pump + "<br/>" + rows.length + "<br/>x -> " + coeObj.linCoe + " x + " + coeObj.absCoe);
            function dataHelper(item, index){
                return {
                    flow : item.absFlowContrib(pump-1),
                    head : item.head[pump-1],
                    row  : item
                };
            }
            var soloPoints  = API.finalTable.pick( soloRows).map(dataHelper);
            var soloX = soloPoints.map(function(d){ return d.flow; });
            var soloY = soloPoints.map(function(d){ return d.head; });
            var xMinSolo = soloX.min();
            var xMaxSolo = soloX.max();
            var yMinSolo = soloY.min();
            var yMaxSolo = soloY.max();
            var comboPoints  = API.finalTable.pick( comboRows).map(dataHelper);
            var comboX = comboPoints.map(function(d){ return d.flow; });
            var comboY = comboPoints.map(function(d){ return d.head; });
            var xMinCombo = comboX.min();
            var xMaxCombo = comboX.max();
            var yMinCombo = comboY.min();
            var yMaxCombo = comboY.max();
            
            var _xMin = Math.min(xMinSolo, xMinCombo);
            var _xMax = Math.max(xMaxSolo, xMaxCombo);
            var _yMin = Math.min(yMinSolo, yMinCombo);
            var _yMax = Math.max(yMaxSolo, yMaxCombo);
            
            // extend a little bit
            var xDiff = _xMax - _xMin;
            var xFactor = 0.4;
            var dx = xFactor * xDiff;
            var xMin = _xMin - dx;
            var xMax = _xMax + dx;
            // y
            var yDiff = _yMax - _yMin;
            var yFactor = 0.4;
            var dy = yFactor * yDiff;
            var yMin = _yMin - dy;
            var yMax = _yMax + dy;
            
            var container = jQuery("#charCurveChartTd");
            container = d3.select(container[0]);
            CHARCURVES.chartContainer = container;
            CHARCURVES._xMin = _xMin;
            CHARCURVES._xMax = _xMax;
            CHARCURVES._yMin = _yMin;
            CHARCURVES._yMax = _yMax;
            CHARCURVES.soloPoints = soloPoints;
            CHARCURVES.comboPoints = comboPoints;
            
            if (!("last_yMin" in CHARCURVES) || !CHARCURVES.yAxisManual){
                CHARCURVES.last_yMin = yMin;
            }
            if (!("last_yMax" in CHARCURVES) || !CHARCURVES.yAxisManual){
                CHARCURVES.last_yMax = yMax;
            }
            if (CHARCURVES.yAxisManual){
                CHARCURVES.makeChart(container, xMin, xMax, CHARCURVES.last_yMin, CHARCURVES.last_yMax);
            } else {
                CHARCURVES.makeChart(container, xMin, xMax, yMin, yMax);
            }

//var charMfgr = mfgrCharCurve[pump-1].map(function(entry){ return [entry[0], API.ftToPsiFactor*entry[1]]; });
        }
    },
    makeXtrafo : function(){
        return d3.scale.linear().range([0, this.innerWidth]).domain([this.xMin, this.xMax]);
    },
    makeYtrafo : function(){
        return d3.scale.linear().range([this.innerHeight, 0]).domain([this.yMin, this.yMax]);
    },
    makeChart : function(container, xMin, xMax, yMin, yMax){
        var soloPoints = this.soloPoints;
        var comboPoints = this.comboPoints;
        var _xMin = this._xMin;
        var _xMax = this._xMax;
        var _yMin = this._yMin;
        var _yMax = this._yMax;
        var pump = this.pump;
        this.xMin = xMin;
        this.xMax = xMax;
        this.yMin = yMin;
        this.yMax = yMax;
        
        container.html("");
        var svg = this.svg = container.append("svg");
        var height0 = this.height0;
        var height = this.height = height0 * this.sizeFactor_Y;
        var width = this.width = height0 * this.aspect * this.sizeFactor_X;
        svg.attr("width", width).attr("height", height).classed("charCurve", true);
        var margTop   = this.margin.top;
        var margBot   = this.margin.bottom;
        var margLeft  = this.margin.left;
        var margRight = this.margin.right;
        var innerWidth = this.innerWidth = width - margLeft - margRight;
        var innerHeight = this.innerHeight = height - margTop - margBot;
        var mainGroup = this.mainGroup = svg.append("g").translate(margLeft, margTop).classed("mainGroup", true);
		this.invisiblePlaceholderRect = mainGroup.append("rect").attr("width", width).attr("height", height).attr("stroke", "none").attr("fill", "transparent");

        var clipPath = this.clipPath = mainGroup.append("clipPath").attr("id", "charCurveClipPath");
        var clipRect = this.clipRect = clipPath.append("rect").attr("width", innerWidth).attr("height", innerHeight).attr("x", 0).attr("y", 0);
        var clipGroup = this.clipGroup = mainGroup.append("g").classed("clipGroup", true)
            .style("clip-path", "url(#charCurveClipPath)");

        
        var x = this.x = this.makeXtrafo();
        var y = this.y = this.makeYtrafo();
        var xAxis = this.xAxis = d3.svg.axis().scale(x).orient("bottom");
        var yAxis = this.yAxis = d3.svg.axis().scale(y).orient("left");
        var xAxisGroup = this.xAxisGroup = mainGroup.append("g").classed("axis xAxis", true).translate(0, innerHeight).call(xAxis);
        var xAxisLabel = this.xAxisLabel = xAxisGroup.append("text").classed("axisLabel xAxisLabel", true)
            .text("flow contribution of pump " + pump + " (MGD)").attr("x", innerWidth/2).attr("y", margBot - this.xAxisLabelVerticalPositionAdjust);
        var yAxisGroup = this.yAxisGroup = mainGroup.append("g").classed("axis yAxis", true).call(yAxis);
        var yAxisLabel = this.yAxisLabel = yAxisGroup.append("text").classed("axisLabel yAxisLabel", true).rotate(-90)
            .text("TDH (psi)").attr("x", -innerHeight/2).attr("y", -margLeft + this.yAxisLabelHorizontalAdjust).style("text-anchor", "middle");
        clipGroup.selectAll(".honestPolitician").data(comboPoints).enter().append("circle").classed("combo", true)
            .attr("cx", function(d) { return x(d.flow); })
            .attr("cy", function(d) { return y(d.head); })
            .attr("r", this.dotRadius)
            .on("mouseover", function(d){
                var row = d.row;
                var containerId = "charCurveMouseOverInfo";
                var chosenColumnIndex = row.pumpsList.indexOf(pump) + 2;
                var tableId = "rightCharCurveHintTable";
                var dragHandleId = "charCurveHintHandle";
                var minimizerId = "charCurveMinimizer";
                var captionId = "charCurveMouseOverCaption"
                makeMouseOverTable(row, chosenColumnIndex, tableId, containerId, dragHandleId, minimizerId, captionId);
            });
        clipGroup.selectAll(".honestPolitician").data(soloPoints).enter().append("circle").classed("solo", true)
            .attr("cx", function(d) { return x(d.flow); })
            .attr("cy", function(d) { return y(d.head); })
            .attr("r", this.dotRadius)
            .on("mouseover", function(d){
                var row = d.row;
                var containerId = "charCurveMouseOverInfo";
                var chosenColumnIndex = row.pumpsList.indexOf(pump) + 2;
                var tableId = "rightCharCurveHintTable";
                var dragHandleId = "charCurveHintHandle";
                var minimizerId = "charCurveMinimizer";
                var captionId = "charCurveMouseOverCaption";
                makeMouseOverTable(row, chosenColumnIndex, tableId, containerId, dragHandleId, minimizerId, captionId);
            });
        var THIS = this;
        d3.select(this.zoomInButton[0]).on("click", function(){
            THIS.zoomIn();
        });
        d3.select(this.zoomOutButton[0]).on("click", function(){
            THIS.zoomOut();
        });
        
        d3.select(this.stretchButton[0]).on("click", function(){
            THIS.stretch();
        });
        d3.select(this.antiStretchButton[0]).on("click", function(){
            THIS.antiStretch();
        });
        svg.call(makePanDragBehavior(this.myName));
        // draw the char curve
        var epsilon = this.epsilonForExtendingTheLineAlittleBitLeftAndRight;
        var pointsOnLine = this.pointsOnLine;
        var xLo = (1+epsilon) * xMin - epsilon * xMax;
        var xHi = (1+epsilon) * xMax - epsilon * xMin;
        var stepSize = (xHi - xLo)/(pointsOnLine-1)*0.9999999;
        var xxx = d3.range(xLo, xHi, stepSize);
        var coeObj = API.formulas.singleCharCurvesFlow2Head[pump-1];
        var charCurve = makeLinearFunction(coeObj);
        var pointsOnCurve = xxx.map(function(x){
            return [x, charCurve(x)];
        });
        charCurveLine = d3.svg.line().x(function(d,i,j){ return x(d[0]); }).y(function(d,i,j){ return y(d[1]); });
        clipGroup.selectAll(".honestPolitician").data([pointsOnCurve]).enter().append("path").attr({
            "fill" : "none", "d" : charCurveLine
        }).classed("charCurveLine", true);
        // mouse lines
        var hMouseLine = this.hMouseLine = mainGroup.append("line")
            .attr("x1", 0).attr("x2", innerWidth).attr("y1", 0).attr("y2", 0)
            .attr("id", "charCurve_H_MouseLine").classed("mouseLine horizontal inactive", true);
        var vMouseLine = this.vMouseLine = mainGroup.append("line")
            .attr("y1", innerHeight).attr("x1", 0).attr("x2", 0)
            .attr("id", "charCurve_V_MouseLine").classed("mouseLine vertical inactive", true);
        var yMouseAxisRectWidth = margLeft - 5;
        var yMouseAxisRectHeight = this.yMouseAxisRectHeight = 18;
        var yMouseAxisRect = this.yMouseAxisRect = svg.append("rect").attr("x", margLeft-yMouseAxisRectWidth).attr("width", yMouseAxisRectWidth)
            .attr("height", yMouseAxisRectHeight).classed("yMouseAxisRect mouseAxisRect inactive", true);
        var yMouseAxisText = this.yMouseAxisText = svg.append("text").attr("x", margLeft - 6).attr("y", 0)
            .classed("yMouseAxisText mouseAxisText inactive", true);
        var xMouseAxisRectWidth = this.xMouseAxisRectWidth = 60;
        var xMouseAxisRectHeight = this.xMouseAxisRectHeight;
        var xMouseAxisRect = this.xMouseAxisRect = svg.append("rect").attr("y", height-margBot).attr("width", xMouseAxisRectWidth).attr("height", xMouseAxisRectHeight)
            .classed("xMouseAxisRect mouseAxisRect inactive", true);
        var xMouseAxisText = this.xMouseAxisText = svg.append("text").attr("y", height-margBot+xMouseAxisRectHeight-this.xMouseAxisTextVerticalAdjust)
            .classed("xMouseAxisText mouseAxisText inactive", true);
        this.attachMouseHandler();
        // legend
        var legendGroup = this.legendGroup = svg.append("g").classed("legendGroup", true).attr("id", "charCurveLegendGroup").translate(width - this.legendWidth, 0);
        legendGroup.append("text").text("solo").classed("charCurveLegendSoloText charCurveLegendText", true).attr("x", 0).attr("y", this.legendLine_1_fromTop);
        legendGroup.append("text").text("in combination").classed("charCurveLegendComboText charCurveLegendText", true).attr("x", 0).attr("y", this.legendLine_2_fromTop);
        mouseZoomInit.call(this);
    },
    attachMouseHandler : function(){
        theBody.on("mousemove", function(){
            var OBJ = CHARCURVES;
            var margTop   = OBJ.margin.top;
            var margBot   = OBJ.margin.bottom;
            var margLeft  = OBJ.margin.left;
            var margRight = OBJ.margin.right;
            var m = d3.mouse(OBJ.mainGroup.node());
            var xRaw = m[0];
            var yRaw = m[1];
            var xRaw_1 = xRaw + margLeft;
            var yRaw_1 = yRaw + margTop;
            var hMouseLine = OBJ.hMouseLine;
            var vMouseLine = OBJ.vMouseLine;
            var yMouseAxisRect = OBJ.yMouseAxisRect;
            var yMouseAxisText = OBJ.yMouseAxisText;
            var xMouseAxisRect = OBJ.xMouseAxisRect;
            var xMouseAxisText = OBJ.xMouseAxisText;
            var innerWidth = OBJ.innerWidth;
            var xInRange = xRaw * (innerWidth - xRaw) > 0;
            var innerHeight = OBJ.innerHeight;
            var yInRange = yRaw * (innerHeight - yRaw) > 0;
            var mouseIsOverChart = xInRange && yInRange;
            hMouseLine.classed("inactive", !mouseIsOverChart);
            vMouseLine.classed("inactive", !mouseIsOverChart);
            yMouseAxisRect.classed("inactive", !mouseIsOverChart);
            yMouseAxisText.classed("inactive", !mouseIsOverChart);
            xMouseAxisRect.classed("inactive", !mouseIsOverChart);
            xMouseAxisText.classed("inactive", !mouseIsOverChart);
            if (mouseIsOverChart){
                hMouseLine.attr("y1", yRaw).attr("y2", yRaw).attr("x2", xRaw);
                vMouseLine.attr("x1", xRaw).attr("x2", xRaw).attr("y2", yRaw);
                yMouseAxisRect.attr("y", yRaw_1 - OBJ.yMouseAxisRectHeight/2);
                yMouseAxisText.attr("y", yRaw_1 + OBJ.yMouseAxisRectHeight/2-3).text("%5.1f".sprintf(OBJ.y.invert(yRaw)));
                xMouseAxisRect.attr("x", xRaw_1 - OBJ.xMouseAxisRectWidth/2);
                xMouseAxisText.attr("x", xRaw_1).text("%5.1f".sprintf(OBJ.x.invert(xRaw)));
            }
        });
    },
    updateChart : function(xMin, xMax, yMin, yMax){
        // use this for speed if data HAS NOT changed, only zoom and/or pan and/or chart size has changed
        var soloPoints = this.soloPoints;
        var comboPoints = this.comboPoints;
        var _xMin = this._xMin;
        var _xMax = this._xMax;
        var _yMin = this._yMin;
        var _yMax = this._yMax;
        var pump = this.pump;
        this.xMin = xMin;
        this.xMax = xMax;
        this.yMin = yMin;
        this.yMax = yMax;

        this.last_yMin = yMin;
        this.last_yMax = yMax;

        var svg = this.svg;
        var height0 = this.height0;
        var height = this.height = height0 * this.sizeFactor_Y;
        var width = this.width = height0 * this.aspect * this.sizeFactor_X;
        svg.attr("width", width).attr("height", height).classed("charCurve", true);
        var margTop   = this.margin.top;
        var margBot   = this.margin.bottom;
        var margLeft  = this.margin.left;
        var margRight = this.margin.right;
        var innerWidth = this.innerWidth = width - margLeft - margRight;
        var innerHeight = this.innerHeight = height - margTop - margBot;
        var mainGroup = this.mainGroup;
        var clipRect = this.clipRect;
        clipRect.attr("width", innerWidth).attr("height", innerHeight);
        var clipGroup = this.clipGroup;
        mainGroup.translate(margLeft, margTop);
        this.invisiblePlaceholderRect.attr("width", width).attr("height", height);
        var x = this.x = this.makeXtrafo();
        var y = this.y = this.makeYtrafo();
        var xAxis = this.xAxis = d3.svg.axis().scale(x).orient("bottom");
        var yAxis = this.yAxis = d3.svg.axis().scale(y).orient("left");
        var xAxisGroup = this.xAxisGroup;
        xAxisGroup.translate(0, innerHeight).call(xAxis);
        var xAxisLabel = this.xAxisLabel;
        xAxisLabel.attr("x", innerWidth/2).attr("y", margBot - this.xAxisLabelVerticalPositionAdjust);
        var yAxisGroup = this.yAxisGroup;
        yAxisGroup.call(yAxis);
        var yAxisLabel = this.yAxisLabel;
        yAxisLabel.attr("x", -innerHeight/2).attr("y", -margLeft + this.yAxisLabelHorizontalAdjust);
        clipGroup.selectAll("circle.combo").data(comboPoints)
            .attr("cx", function(d) { return x(d.flow); })
            .attr("cy", function(d) { return y(d.head); });
        clipGroup.selectAll("circle.solo").data(soloPoints)
            .attr("cx", function(d) { return x(d.flow); })
            .attr("cy", function(d) { return y(d.head); });
        // update the char curve
        var epsilon = this.epsilonForExtendingTheLineAlittleBitLeftAndRight;
        var pointsOnLine = this.pointsOnLine;
        var xLo = (1+epsilon) * xMin - epsilon * xMax;
        var xHi = (1+epsilon) * xMax - epsilon * xMin;
        var stepSize = (xHi - xLo)/(pointsOnLine-1)*0.9999999;
        var xxx = d3.range(xLo, xHi, stepSize);
        var coeObj = API.formulas.singleCharCurvesFlow2Head[pump-1];
        var charCurve = makeLinearFunction(coeObj);
        var pointsOnCurve = xxx.map(function(x){
            return [x, charCurve(x)];
        });
        charCurveLine = d3.svg.line().x(function(d,i,j){ return x(d[0]); }).y(function(d,i,j){ return y(d[1]); });
        clipGroup.selectAll("path.charCurveLine").data([pointsOnCurve]).attr({
            "fill" : "none", "d" : charCurveLine
        }).classed("charCurveLine", true);
        var xMouseAxisRectHeight = this.xMouseAxisRectHeight;
        this.xMouseAxisRect.attr("y", height-margBot);
        this.xMouseAxisText.attr("y", height-margBot+xMouseAxisRectHeight-this.xMouseAxisTextVerticalAdjust);
        this.vMouseLine.attr("y1", innerHeight);
        this.legendGroup.translate(width - this.legendWidth, 0);
    },
    onActivate : function(){
        this.attachMouseHandler();
        //debugger;
        initChartResizing(this.myName);
    },
    onDeactivate : function(){
        //debugger
        this.chartResizerElt.style("display", "none");
    }
};
